resource "aws_default_security_group" "default-myapp-sg" {

  vpc_id = var.vpc_id

  ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]

  }

  ingress {
    description = "internet from VPC"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Name = "${var.env_prefix}-default-sg"
  }
}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = [var.image_name]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

/*resource "aws_key_pair" "ssh-key" {
  key_name   = "server-key"
  public_key = "${file(var.public_key_location)}"
}*/

resource "aws_instance" "myapp-ec2" {
  ami                         = data.aws_ami.latest-amazon-linux-image.id
  instance_type               = var.instance_type
  subnet_id                   = var.subnet_id
  vpc_security_group_ids      = [aws_default_security_group.default-myapp-sg.id]
  availability_zone           = var.avail_zone
  associate_public_ip_address = true
  #key_name = aws_key_pair.ssh-key.key_name
  key_name = "module"
  #user_data = file("script.sh")

  tags = {
    Name = "${var.env_prefix}-ec2"
  }

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file(var.public_key_location) # Remplacez par le chemin de votre clé privée
    host        = self.public_ip
  }


  provisioner "file" {
    source      = "../script.sh"
    destination = "/home/ec2-user/script-server.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/ec2-user/script-server.sh",
      "sudo /home/ec2-user/script-server.sh"
    ]
  }

  provisioner "local-exec" {
    command = "echo ${self.public_ip} > oupout.txt"

  }
}